<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gazi_com');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',a.RDd6i7<,hg9Nm[X.NTLG*JTFs 1XE7`L4G`(+N?`Mqqk{^[Eok=dC uGaDdBP');
define('SECURE_AUTH_KEY',  'eL->nlDP.HS98xf}ku<GMT_349pQHcFS %P-HA:SH627`:@6q*&VN/ON.nZfKuvh');
define('LOGGED_IN_KEY',    'WPL6+bX4V7V_(@gTKEK&Gmz60+pn=<6(P4I:dPI5?y#7i44vFaM3B*K?U)t+Zc%o');
define('NONCE_KEY',        'Al+R0?)lM|:LFg`8cd=AN[a|0&p6oPNK|yji2tl2{A/eXQP}k@e]Ohq_7aEzsUCO');
define('AUTH_SALT',        '-CimvV;r>M)Q#.a{by({jh-[nENCih<2H-w6}VIfRH vlFM^uHn,2m#(M V2d0.e');
define('SECURE_AUTH_SALT', 'Nrn6*#Mw-o *]|#K_?fq2>=L]1a1VZ#VCN><fS)@j[]:9w{cO:%-?+Res{01~<@_');
define('LOGGED_IN_SALT',   '8(ak6q;ub$q]A=(xA+ !IMZd|F6t>|e+#6y`(~Nmc:6K@)EcQRv;z:iaNRjrL}4[');
define('NONCE_SALT',       ']*!]nQ+][hVo bO*U)4~346_?KvB-U6<{Wy%=|RrDCtm9uCBfxWf-p.}$a!dZb:j');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
//**********   disable WP Post Revisions ********  newly added
define('AUTOSAVE_INTERVAL', 300 ); // seconds
define('WP_POST_REVISIONS', false );

/*Disable auto update*/
define( 'WP_AUTO_UPDATE_CORE', false );
